﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Join_DK : MonoBehaviour {

	public InputField idField;
	public InputField pwField;
	public InputField nnField;



	void Update(){
		if (idField.isFocused == true) {
			if (Input.GetKeyDown (KeyCode.Tab)) {
				pwField.Select ();
				pwField.ActivateInputField ();
			}
		}

		if (pwField.isFocused == true) {
			if (Input.GetKeyDown (KeyCode.Tab)) {
				nnField.Select ();
				nnField.ActivateInputField ();
			}
		}
	}
}
