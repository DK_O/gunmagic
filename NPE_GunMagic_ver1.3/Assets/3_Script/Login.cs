﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Login : MonoBehaviour {

	public string inputID;
	public string inputPW;

	string LoginURL = "http://reach-db.comeze.com/login.php";

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.L)) {
			StartCoroutine(LoginToDB (inputID, inputPW));
		}
	}


	IEnumerator LoginToDB(string username, string password){
		WWWForm form = new WWWForm ();
		form.AddField ("userIDPost", username);
		form.AddField ("userPWPost", password);

		WWW www = new WWW (LoginURL, form);
		yield return www;
		Debug.Log (www.text);
	}
}
