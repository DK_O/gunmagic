﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemManager_LKR : MonoBehaviour {

    /* 주석 작성일자 : 2017/04/13 오후 3시즈음
     * 플레이어한테 달아주는 스크립트.
     * 여기에서 플레이어와 닿은 아이템이 어떤것인지 체크하고, 각 아이템의 기능을 실행시키게 할 것임.
     * 0413 체력관련된 부분은 일단 보류.
     * 0413 체력 이외의 아이템을 체크해서 로그 띄워주고 이펙트 띄워주는 기능 잘 작동함.
     * 0413 로그가 두번 찍힘... 해결해봐야할듯.
     * 0413 로그 두번 찍히던거 해결..방향 알려주는 큐브때문에 생긴 문제인듯.
     */

    public GameObject eff_heartItem;
    public GameObject eff_damage;
    public GameObject eff_manaItem;

    //public Text tx_hp;
    //public Text tx_mp;

    int hp_num, mp_num;

    // Use this for initialization
    void Start()
    {
		
    }

    void OnTriggerEnter(Collider other) // 플레이어와 닿은 오브젝트가 other로 들어감.
    {
		if(other.gameObject.tag == "HeartItem")
		{
			Destroy(other.gameObject);

			GameObject e = Instantiate(eff_heartItem, other.gameObject.transform.position, other.gameObject.transform.rotation);
			Destroy(e, 0.5f); //이펙트가 루프 한번 돌았을 때나 적당히 화면에 보여줬을 때 삭제함.
		}
		else if(other.gameObject.tag == "ManaItem")
		{
			Destroy(other.gameObject);

			GameObject e = Instantiate(eff_manaItem, other.gameObject.transform.position, other.gameObject.transform.rotation);
			Destroy(e, 2.0f);
		}
		else if(other.gameObject.tag == "Damage")
		{
			Destroy(other.gameObject);

			GameObject e = Instantiate(eff_damage, other.gameObject.transform.position, other.gameObject.transform.rotation);
			Destroy(e, 1.0f);

		}
    }
}
