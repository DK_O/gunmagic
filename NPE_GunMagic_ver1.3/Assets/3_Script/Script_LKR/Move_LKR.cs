﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move_LKR : MonoBehaviour {

    /* 주석 작성일자 : 2017/04/13 오후 3시40분즈음
     * 이 스크립트에서 플레이어의 움직임을 담당.
     * 아직 점프 기능은 없음. 점프 기능은 한번만 점프하게 할 예정, 공중에서 점프 못하게 할 예정(2단점프 불가능하게).
     */

    public float speed = 8.0f;
	public float jumpPower = 500;
    Vector3 look;
	Animator anim;
	Rigidbody m_rb;

	public GameObject chatBox;
	ChatBoxFunctions m_chat;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
		m_chat = chatBox.gameObject.GetComponent<ChatBoxFunctions> ();
		m_rb = gameObject.GetComponent<Rigidbody> ();
	}

    void FixedUpdate()
    {
		if(!m_chat.isChat){
			if (Input.GetKey (KeyCode.UpArrow) || Input.GetKey (KeyCode.DownArrow) || Input.GetKey (KeyCode.LeftArrow) || Input.GetKey (KeyCode.RightArrow)) {
				float x = Input.GetAxis ("Horizontal");
				float z = Input.GetAxis ("Vertical");
				look = x * Vector3.right + z * Vector3.forward;

				transform.rotation = Quaternion.LookRotation (look);
				transform.Translate (Vector3.forward * speed * Time.deltaTime);

				anim.SetBool ("isWalking", true);
			} else {
				anim.SetBool ("isWalking", false);
			}
		}

		if(Input.GetKeyDown (KeyCode.Space)){
			m_rb.AddForce (transform.up * jumpPower);
		}
    }
}
