﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCam_LKR : MonoBehaviour {

    /* 주석 작성일자 : 2017/04/17 오후 3시30분 즈음
     * 바라볼 대상을 따라서 다니는 스크립트.
     * 특정 위치에 가면 카메라의 이동 제한됨.
     */

    public Transform target; // 바라볼 대상
    public float dist = 10.0f;
    public float height = 5.0f;

    public float xMin = -30.0f;
    public float xMax = 30.0f;
    public float zMin = -30.0f;
    public float zMax = 30.0f;

    Transform tr; // 카메라

	// Use this for initialization
	void Start () {
        tr = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void LateUpdate () {
        // 카메라 위치를 dist 만큼 띄우고, 높이를 올리기
        tr.transform.position = target.position - (Vector3.forward * dist) + (Vector3.up * height);
        // 타겟을 바라보게 하기
        tr.transform.LookAt(target);

        if (this.transform.position.x > xMax) //이 스크립트가 붙어있는 오브젝트의 위치값 x가 30을 넘으면
        {
            this.transform.position = new Vector3(xMax, this.transform.position.y, this.transform.position.z); // 오브젝트의 x값을 30으로 고정. 이하 비슷하게 동일.
        }
        if (this.transform.position.x < xMin)
        {
            this.transform.position = new Vector3(xMin, this.transform.position.y, this.transform.position.z);
        }
        if (this.transform.position.z > zMax)
        {
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, zMax);
        }
        if (this.transform.position.z < zMin)
        {
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, zMin);
        }
	}
}
