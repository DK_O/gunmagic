﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WindowsInput;

public class PlayerAttack_LKR : MonoBehaviour {

    /* 주석 작성일자 : 2017/04/13 오후3시45분즈음
     * 이 스크립트에서 플레이어의 공격(아직은 마법 스킬..)기능을 구현했음.
     * 총 공격도 여기에서 처리하면 될 듯.(아마 마법 스킬을 총 공격으로 대처하면 될듯.)
     * 마법공격은 아마 키보드 키를 다른 걸로 바꿔서 설정.
     */

    public GameObject eff_magic;
    public Transform shootPos;
    public float range = 10.0f;

	Animator anim;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        //Debug.DrawRay(transform.position, transform.forward * range, Color.red);
		if (WinInput.GetKey(KeyCode.L)) { // 마우스 왼쪽 버튼으로 발사
			GameObject e = Instantiate (eff_magic, shootPos.position, shootPos.rotation);
			RaycastHit hit;
			anim.SetBool ("isAttack", true);
			//현재 위치에서 앞 방향으로 hit을 range 범위 만큼 쏜 다면
			if (Physics.Raycast (transform.position, transform.forward, out hit, range)) {
				if (hit.collider.gameObject.tag == "Enemy") {//여기서 부딛힌 hit의 정보를 가져와서 체력-- 해주면 됨.
					//hit.rigidbody.AddForce(transform.forward * power);
					Debug.Log ("HitHitHit!");
				}
			}
			Destroy (e, 1.5f);
		} else {
			anim.SetBool ("isAttack", false);
		}
    }
}
