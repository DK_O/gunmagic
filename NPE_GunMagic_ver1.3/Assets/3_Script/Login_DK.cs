﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;


public class Login_DK : MonoBehaviour {

	// 오동근이 작성하였음...

	public Button loginButton;

	public InputField idField;
	public InputField pwField;

	public Text IDtext;
	public Text PWtext;

	public Text Join_IDtext;
	public Text Join_PWtext;
	public Text Join_NNtext;

	public Text WarningText;
	public Text Join_WarningText;

	string inputID;
	string inputPW;

	string Join_inputID;
	string Join_inputPW;
	string Join_inputName;

	string LoginURL = "http://reach-db.comeze.com/login.php";
	string CreateUserURL = "http://reach-db.comeze.com/insertUser2.php";
	string loadUserURL = "http://reach-db.comeze.com/loadUser.php";

	public Light directional;

	void Start(){
		directional.gameObject.SetActive (false);
		directional.gameObject.SetActive (true);
	}

	void Update(){
		if (idField.isFocused == true) {
			if (Input.GetKeyDown (KeyCode.Tab)) {
				pwField.Select ();
				pwField.ActivateInputField ();
			}
		}
		if(IDtext.text != "" && PWtext.text != ""){
			if (Input.GetKeyDown (KeyCode.Return)) {
				loginButton.onClick.Invoke ();
			}
		}

	}

	public void JoinClick(Animator anim){
		anim.SetBool ("isJoining", true);
	}

	public void JoinOutClick(Animator anim){
		anim.SetBool ("isJoining", false);
	}
	public void LoginClick(){
		inputID = IDtext.text;
		inputPW = PWtext.text;
		StartCoroutine(LoginToDB (inputID, inputPW));
		StartCoroutine (loadUserName (inputID));
	}

	public void UserInsert(){
		Join_inputID = Join_IDtext.text;
		Join_inputPW = Join_PWtext.text;
		Join_inputName = Join_NNtext.text;

		if(Join_inputID.Length > 0 && Join_inputPW.Length >0 && Join_inputName.Length >0){
			if (Join_inputID.Length < 11 && Join_inputPW.Length < 11 && Join_inputName.Length < 9) {
				Join_WarningText.color = Color.white;
				Join_WarningText.text = "로딩중...";
				StartCoroutine (CreateUser (Join_inputID, Join_inputPW, Join_inputName));
			} else {
				Join_WarningText.color = Color.red;
				Join_WarningText.text = "잘못된 값입니다.";
			}
		} else {
			Join_WarningText.color = Color.red;
			Join_WarningText.text = "잘못된 값입니다.";
		}
	}


	IEnumerator LoginToDB(string username, string password){
		WWWForm form = new WWWForm ();
		form.AddField ("userIDPost", username);
		form.AddField ("userPWPost", password);

		WarningText.color = Color.black;
		WarningText.text = "로딩중...";
		WWW www = new WWW (LoginURL, form);
		yield return www;
		Debug.Log (www.text);
		if (www.text.Contains ("login")) {
			Debug.Log ("로드씬");
			SceneManager.LoadScene (1);
			// 유저명확인 및 playerpref에 저장
		} else {
			WarningText.color = Color.red;
			WarningText.text = "잘못된 값입니다.";
		}
	}

	IEnumerator CreateUser(string username, string password, string name){
		WWWForm form = new WWWForm ();
		form.AddField ("userIDPost", username);
		form.AddField ("userPWPost", password);
		form.AddField ("userNamePost", name);

		WWW www = new WWW (CreateUserURL, form);
		yield return www;
		Debug.Log (www.text);
		//아래 세분화
		if(www.text.Contains ("Exist")){
			Join_WarningText.color = Color.red;
			Join_WarningText.text = "이미 존재하는 ID 입니다.";
		} else {
			Join_WarningText.color = Color.blue;
			Join_WarningText.text = "회원가입 완료 : " + username;
		}

	}

	IEnumerator loadUserName (string username){
		WWWForm form2 = new WWWForm();
		form2.AddField ("userIDPost", username);
		WWW www2 = new WWW (loadUserURL, form2);
		yield return www2;
		Debug.Log (www2.text);
		PlayerPrefs.SetString ("Name",www2.text);
	}
}
