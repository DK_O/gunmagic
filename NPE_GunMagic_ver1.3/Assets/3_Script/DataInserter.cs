﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataInserter : MonoBehaviour {

	public string inputID;
	public string inputPW;
	public string inputName;

	string CreateUserURL = "http://reach-db.comeze.com/insertUser.php";

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
			CreateUser (inputID, inputPW, inputName);
		}
	}

	public void CreateUser(string username, string password, string name){
		WWWForm form = new WWWForm ();
		form.AddField ("userIDPost", username);
		form.AddField ("userPWPost", password);
		form.AddField ("userNamePost", name);

		WWW www = new WWW (CreateUserURL, form);
	}
}
