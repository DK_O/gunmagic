﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataLoader : MonoBehaviour {

	public string[] users;

	// Use this for initialization
	IEnumerator Start () {
		WWW userData = new WWW ("http://reach-db.comeze.com/userData.php");
		yield return userData;
		string userDataString = userData.text;
		print (userDataString);
		users = userDataString.Split (';');
		print (GetDataValue(users[1], "Name:"));
	}

	string GetDataValue(string data, string index){
		string value = data.Substring (data.IndexOf(index) + index.Length);
		if (value.Contains ("|")) {
			value = value.Remove (value.IndexOf ("|"));
		}
		return value;
	}

}
