﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatBoxFunctions : MonoBehaviour {

	//오동근이 작성한 코드

	[SerializeField] ContentSizeFitter contentSizeFitter;
	[SerializeField] Text showHideText;
	[SerializeField] Transform messageParentPanel;
	[SerializeField] GameObject messagePrefab;
	[SerializeField] Button sendButton;
	public InputField m_inputfield;


	public Canvas canvas; //말풍선 캔버스
	public Transform target;
	float height = 5.0f;
	public Text BubbleText;

	bool isRunning = false;
	public bool isChat = false;



	bool isChatShowing = false;
	string message = "";

	void Start (){
		canvas.enabled = false; //말풍선 안보임 ㅎㅎ
		ToggleChat();
		ToggleChat();
	}

	void Update(){ //엔터치면 ToggleChat 호출
		if(Input.GetKeyDown(KeyCode.Return)){
			if (!isChatShowing) {
				isChat = true;
				ToggleChat ();
				// if문 만들어서 주문시전하는 코드 작성할 부분
				// message 변수 이용
			} else if(isChatShowing){
				
				sendButton.onClick.Invoke (); // 버튼을 클릭하게 해주는 함수
				ToggleChat ();
				isChat = false;
				// 버튼 눌러서 하면 isChat false 안됨...

			}
		}
	}

	void LateUpdate(){ //말풍선이 캐릭터를 따라다니게
		canvas.transform.position = target.position + (Vector3.up * height);
	}

	public void ToggleChat(){
		
		isChatShowing = (isChatShowing) ? false : true; // true false 뒤집기
		//isChatShowing = !isChatShowing;
		if (isChatShowing) { // 채팅창을 열고 인풋필드에 포커스
			contentSizeFitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
			showHideText.text = "Hide Chat";
			m_inputfield.Select ();
			m_inputfield.ActivateInputField ();

			Debug.Log ("Enter1");
		} else { //채팅창을 닫고 인풋필드에 포커스 OFF
			contentSizeFitter.verticalFit = ContentSizeFitter.FitMode.MinSize;
			m_inputfield.DeactivateInputField ();
			showHideText.text = "Show Chat";
			Debug.Log ("Enter2");
		}
	}

	public void SetMessage(string message){ 
		this.message = message;
	}

	public void ShowMessage(){
		if (message != "") {
			//인풋필드에 입력한 값을 프리팹을 통해 채팅창에 띄움
			GameObject clone = (GameObject)Instantiate (messagePrefab);
			clone.transform.SetParent (messageParentPanel);
			clone.transform.SetSiblingIndex (messageParentPanel.childCount - 2);
			clone.GetComponent<MessageFunctions> ().ShowMessage (message);

			//메시지가 3개 이상일 때 채팅했을 경우 가장 처음 채팅한 것을 삭제
			GameObject[] thingyToFind = GameObject.FindGameObjectsWithTag ("message");
			int thingyCount = thingyToFind.Length;
			Debug.Log (thingyCount);
			if (thingyToFind.Length > 3) {
				Destroy (thingyToFind [0].gameObject); 
			}
				
			if(isRunning){
				StopCoroutine("Blink");
			}
				StartCoroutine("Blink");
		}
	}

	IEnumerator Blink() { //말풍선이 시간이 지나면 사라지게 하기
		isRunning = true;
		BubbleText.text = message;
		canvas.enabled = true;
		yield return new WaitForSeconds(2.0f);
		canvas.enabled = false;
		isRunning = false;
	}


}
