﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameSelectManager : MonoBehaviour {

	public Text NameText;

	string playerName;


	void Start(){
		playerName = PlayerPrefs.GetString ("Name", "");
		NameText.text = playerName;
	}

	public void Stage0(){
		SceneManager.LoadScene (2);
	}

	//public void Stage1(){
	//	SceneManager.LoadScene (3);
	//}

	public void logout(){
		SceneManager.LoadScene (0);
	}
}
