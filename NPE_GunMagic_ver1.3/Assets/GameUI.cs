﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameUI : MonoBehaviour {

	public GameObject pausePanel;

	public void Start(){
		pausePanel.SetActive (false);
	}

	public void pause(){
		//패널 Active
		Time.timeScale = 0;
		pausePanel.SetActive (true);
	}

	public void unpause(){
		pausePanel.SetActive (false);
		Time.timeScale = 1;
	}

	public void loadMain(){
		Time.timeScale = 1;
		SceneManager.LoadScene (1);
	}
}
