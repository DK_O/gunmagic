﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animControll : MonoBehaviour {

	public float speed;
	Vector3 look;

	void Start () {
		speed = 10.0f;
	}
	
	// Update is called once per frame
	void Update () {
       
    }
	void FixedUpdate()
	{
		if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
		{
			float x = Input.GetAxis("Horizontal");
			float z = Input.GetAxis("Vertical");
			look = x * Vector3.right + z * Vector3.forward;

			transform.rotation = Quaternion.LookRotation(look);
			transform.Translate(Vector3.forward * speed * Time.deltaTime);
		}

	}
}