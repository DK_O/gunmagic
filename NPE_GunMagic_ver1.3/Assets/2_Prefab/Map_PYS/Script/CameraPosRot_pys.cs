﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPosRot_pys : MonoBehaviour {
	public GameObject pos1;
	public float rotSpeed = 1.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		cameraPosRot ();
	}

	void cameraPosRot(){

		gameObject.transform.RotateAround(gameObject.transform.position, Vector3.right, rotSpeed);
		gameObject.transform.RotateAround(gameObject.transform.position, Vector3.up, rotSpeed);

	}


}
