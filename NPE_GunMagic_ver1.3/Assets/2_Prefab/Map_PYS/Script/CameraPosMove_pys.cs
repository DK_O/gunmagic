﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPosMove_pys : MonoBehaviour {
	public GameObject cameraPos1mr;
	public GameObject cameraPos2m;
	public GameObject cameraPos3r;
	public GameObject cameraPos4m;
	public GameObject cameraPos5r;
	public GameObject cameraPos6m;
	public GameObject cameraPos7r;
	public GameObject cameraPos8m;
	public float moveSpeed = 10.0f;
	public float rotationSpeed = 3.0f;
	bool pos1 = false;
	bool pos2 = true;
	bool pos3 = false;
	bool pos4 = false;
	bool pos5 = false;
	bool pos6 = false;
	bool pos7 = false;
	bool pos8 = false;

	// Use this for initialization
	void Start () {
		gameObject.transform.position = new Vector3 (-789.06f, 625.284f, -721.7762f);
		
	}
	
	// Update is called once per frame
	void Update () {
		if(pos2 == true){
			cameraPosMove2();
		}
		if(pos4 == true){
			cameraPosMove4();
		}
		if(pos6 == true){
			cameraPosMove6();
		}
		if(pos8 == true){
			cameraPosMove8();
		}
		
	}

//	void cameraPosMove1(){ //(-789.06f, 625.284f, -721.7762f) P
//		gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, cameraPos1mr.transform.position, moveSpeed * Time.deltaTime);
//		if (Vector3.Distance (transform.position, cameraPos1mr.transform.position) <= 0.5f) {
//			pos1 = false;
//			pos2 = true;
//			pos3 = false;
//			pos4 = false;
//			pos5 = false;
//			pos6 = false;
//			pos7 = false;
//			pos8 = false;
//			pos9 = false;
//		}
//	}

	void cameraPosMove2(){ //(-500.06f, 625.284f, -721.7762f) P //(45.379f, 1.72f, 0.001f) R
		gameObject.transform.rotation = Quaternion.Euler (45.379f, 1.72f, 0.001f);
		gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, cameraPos2m.transform.position, moveSpeed * Time.deltaTime);
		if (Vector3.Distance (transform.position, cameraPos2m.transform.position) <= 0.5f) {
			pos1 = false;
			pos2 = false;
			pos3 = false;
			pos4 = true;
			pos5 = false;
			pos6 = false;
			pos7 = false;
			pos8 = false;
		}
	}

	void cameraPosMove3(){ //(45.379f, -100f, 0.001f) R
//		gameObject.transform.Rotate(45.379f, rotationSpeed * Time.deltaTime, 0.001f) ;
//		if (gameObject.transform.rotation.y <= -100f) {
//			pos1 = false;
//			pos2 = false;
//			pos3 = false;
//			pos4 = true;
//			pos5 = false;
//			pos6 = false;
//			pos7 = false;
//			pos8 = false;
//		}
	}

	void cameraPosMove4(){ //(-500.06f, 625.284f, -500.7762f) P //(45.379f, -100f, 0.001f) R
		gameObject.transform.rotation = Quaternion.Euler (45.379f, -100f, 0.001f);
		gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, cameraPos4m.transform.position, moveSpeed * Time.deltaTime);
		if (Vector3.Distance (transform.position, cameraPos4m.transform.position) <= 0.5f) {
			pos1 = false;
			pos2 = false;
			pos3 = false;
			pos4 = false;
			pos5 = false;
			pos6 = true;
			pos7 = false;
			pos8 = false;
		}
	}

	void cameraPosMove6(){ //(-789.06f, 625.284f, -500.7762f) P // (45.379f, -150f, 0.001f) R
		gameObject.transform.rotation = Quaternion.Euler (45.379f, -150f, 0.001f);
		gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, cameraPos6m.transform.position, moveSpeed * Time.deltaTime);
		if (Vector3.Distance (transform.position, cameraPos6m.transform.position) <= 0.5f) {
			pos1 = false;
			pos2 = false;
			pos3 = false;
			pos4 = false;
			pos5 = false;
			pos6 = false;
			pos7 = false;
			pos8 = true;
		}
	}

	void cameraPosMove8(){ //(-789.06f, 625.284f, -721.7762f) P //(45.379f, 100, 0.001f) R
		gameObject.transform.rotation = Quaternion.Euler (45.379f, 100f, 0.001f);
		gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, cameraPos8m.transform.position, moveSpeed * Time.deltaTime);
		if (Vector3.Distance (transform.position, cameraPos8m.transform.position) <= 0.5f) {
			pos1 = false;
			pos2 = true;
			pos3 = false;
			pos4 = false;
			pos5 = false;
			pos6 = false;
			pos7 = false;
			pos8 = false;
		}
	}


}
