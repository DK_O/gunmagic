﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterShot_pys : MonoBehaviour { // 패트롤용
    public GameObject Monster;
	public GameObject MonsterMissile;
	public Transform ShootPos;
	public float shootDelay = 1.0f;
	float shootTimer = 0;

	// Use this for initialization
	void Start () {

    }

	// Update is called once per frame
	void Update () {	
		if (Monster.GetComponent<PatrolBehavior_pys> ().isSearch == true) {
			MonserShooting();
		}
	}

	void MonserShooting(){		
		if (shootTimer > shootDelay) {
            Instantiate(MonsterMissile, transform.position, transform.rotation);
            //Instantiate (MonsterMissile, transform.position, Quaternion.identity);
            //Instantiate (MonsterMissile, ShootPos.localPosition, this.transform.localRotation);
            shootTimer = 0;
		}
		shootTimer += Time.deltaTime;
	}


}
