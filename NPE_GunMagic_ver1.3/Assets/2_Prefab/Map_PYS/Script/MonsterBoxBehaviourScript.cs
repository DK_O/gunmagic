﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterBoxBehaviourScript : MonoBehaviour {
    // 상자몬스터. 맞으면 따라오기

    enum MONSTER_STATE
    {
        REST,
        IDLE,
        MOVE,
        ATTACK,
        DAMAGE,
        DEAD
    }

    MONSTER_STATE state;
    CharacterController charController;
    Animation ani;
    Transform player;
    float stateTime;
    float idleTime = 1.0f;
    public float attackRange = 2.0f;
    public float monsterSpeed = 3.0f;
    public float rotationSpeed = 5.0f;
    public float attackSpeed = 2.0f;
    float recoverTime = 2.0f;
    public int hp = 5;
    bool monsterMove = false;
    bool isCalled = false; // 한번만 호출하기 위한 체크용 변수

    // Use this for initialization
    void Start () {
        state = MONSTER_STATE.REST;
        charController = GetComponent<CharacterController>();
        ani = GetComponent<Animation>();
        player = GameObject.Find("Player").transform;

    }
	
	// Update is called once per frame
	void Update () {
        switch (state)
        {
            case MONSTER_STATE.REST:
                {
                    ani.CrossFade("Rest");
                    if (monsterMove == true)
                    {
                        state = MONSTER_STATE.IDLE;
                    }

                }
                break;
            case MONSTER_STATE.IDLE:
                {
                    stateTime = 0;
                    state = MONSTER_STATE.MOVE;
                    ani.CrossFade("Idle");

                }
                break;
            case MONSTER_STATE.MOVE:
                {
                    float distance = (player.position - transform.position).magnitude;
                    if (distance < attackRange)
                    {
                        ani.CrossFade("Run");
                        state = MONSTER_STATE.ATTACK;
                        stateTime = attackSpeed;
                    }
                    else
                    {
                        ani.CrossFade("Run");
                        Vector3 dir = player.position - transform.position;
                        dir.y = 0;
                        dir.Normalize();
                        charController.SimpleMove(dir * monsterSpeed);
                        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(dir), rotationSpeed * Time.deltaTime);
                    }
                }
                break;
            case MONSTER_STATE.ATTACK:
                {
                    stateTime += Time.deltaTime;
                    if (stateTime > attackSpeed)
                    {
                        stateTime = 0;
                        ani.CrossFade("BiteAttack");
                    }
                    float distance = (player.position - transform.position).magnitude;
                    if (distance > attackRange)
                    {
                        state = MONSTER_STATE.IDLE;
                    }
                }
                break;
            case MONSTER_STATE.DAMAGE:
                {
                    //ani.SetTrigger("hitMob");
                    ani.CrossFade("TakeDamage");
                    state = MONSTER_STATE.IDLE;

                    //if (ani.GetBool("isMove"))
                    //{
                    //   ani.SetBool("isMove", false);
                    //}
                    stateTime = idleTime - recoverTime;
                    hp--;
                    if (hp <= 0)
                    {
                        state = MONSTER_STATE.DEAD;
                    }
                }
                break;
            case MONSTER_STATE.DEAD:
                {
                    ani.CrossFade("Die");
                }
                break;
            default:
                break;

        }
    }
    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "PlayerShot")
        {
            monsterMove = true;
            if (isCalled == false)
            {
                isCalled = true;
                ani.CrossFade("WakeUp");
            }


        }

    }


}
