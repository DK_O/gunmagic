﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolBehavior_pys : MonoBehaviour {    
    public GameObject wayPoint_a;
	public GameObject wayPoint_z;
    public float moveSpeed = 2.0f;
    public float rotSpeed = 2.0f; 
	public float rotationSpeed = 5.0f;
	public float velocity;
	public float accelaration;    
	public bool isSearch = false;
	bool isWayPoint = false;
	Transform player;
	CharacterController charController;
	Animator anitr;



    // Use this for initialization
    void Start () {

		player = GameObject.Find("Player").transform;
		charController = GetComponent<CharacterController>();
		anitr = GetComponent<Animator>();

		accelaration = 0.1f;
		velocity = (velocity + accelaration * Time.deltaTime);

    }
	
	// Update is called once per frame
	void Update () {
		
		if (isSearch == true) {
			Attack ();
		} else {
			//anitr.GetBool("WalkForward");
			WayPiontMove();
		}        
        
	}

    void WayPiontMove(){
		
        if(isWayPoint == false)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(wayPoint_a.transform.position - transform.position), 1);
            transform.Translate(Vector3.forward * Time.deltaTime * moveSpeed);
            
            if (Vector3.Distance(transform.position, wayPoint_a.transform.position) <= 0.5f) isWayPoint = true;
        } else
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(wayPoint_z.transform.position - transform.position), 1);
            transform.Translate(Vector3.forward * Time.smoothDeltaTime * moveSpeed);
            if (Vector3.Distance(transform.position, wayPoint_z.transform.position) <= 0.5f) isWayPoint = false;

        }

		Search();
    }

	void Search(){

		float distance = Vector3.Distance (player.transform.position, transform.position);
		if (distance <= 15)
			isSearch = true;
	}

	void Attack(){

		//anirt.???
		transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(player.transform.position - transform.position), Time.smoothDeltaTime * 3.0f);
		transform.Translate(Vector3.forward * Time.smoothDeltaTime * moveSpeed);
		Vector3 dir = player.position - transform.position;
		float distance = Vector3.Distance (player.transform.position, transform.position);

		if (distance > 15) {
			isSearch = false;

		}
			
	}
		

}
