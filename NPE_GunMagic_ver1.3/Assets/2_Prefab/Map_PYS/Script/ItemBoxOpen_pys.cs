﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBoxOpen_pys : MonoBehaviour {
    public GameObject HeartItem;
    Animation ani_ItemBoxOpen;
    bool isCalled = false; // 한번만 호출하기 위한 체크용 변수

    // Use this for initialization
    void Start () {
        ani_ItemBoxOpen = GetComponent<Animation>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "PlayerShot")
        {
            ani_ItemBoxOpen.CrossFade("Opening");
            StartCoroutine(ShowOpeningDelay()); // 1초짜리인 상자오픈 애니메이션을 보기위한 1.1초 딜레이 코루틴
            if(isCalled == false)
            {
                isCalled = true;
                StartCoroutine(CreateItemDelay());  // 상자가 열린 후 아이템이 등장하기 위한 0.4초 딜레이 코루틴
            }
            
        }
    }

    IEnumerator ShowOpeningDelay()
    {
        yield return new WaitForSeconds(1.1f);
        Destroy(gameObject);        
    }

    IEnumerator CreateItemDelay()
    {
        yield return new WaitForSeconds(0.4f);
        Vector3 temp = new Vector3(transform.position.x, transform.position.y + 0.6f, transform.position.z);
        //Instantiate(HeartItem, gameObject.GetComponent<Transform>().position, gameObject.GetComponent<Transform>().rotation);
        Instantiate(HeartItem, temp, transform.rotation);
    }
}
