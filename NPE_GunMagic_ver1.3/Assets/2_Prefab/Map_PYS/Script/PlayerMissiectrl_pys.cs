﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMissiectrl_pys : MonoBehaviour {
	float shotSpeed = 15.0f;
	public int damage = 1;

	// Use this for initialization
	void Start () {
        GetComponent<Rigidbody>().velocity = transform.forward * shotSpeed * 1;
        StartCoroutine (createMissile ());
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator createMissile(){		
		yield return new WaitForSeconds(0.8f);
		Destroy(gameObject);     
	}
}
