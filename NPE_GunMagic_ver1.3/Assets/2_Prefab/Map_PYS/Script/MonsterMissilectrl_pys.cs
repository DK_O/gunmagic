﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterMissilectrl_pys : MonoBehaviour {
	public float shotSpeed;
	public int damage = 1;

	// Use this for initialization
	void Start () {
        if(gameObject.tag == "Patrol")
        {
            shotSpeed = 1000.0f;
        } else if(gameObject.tag == "NoneMoveMonster")
        {
            shotSpeed = 700.0f;
        }

        GetComponent<Rigidbody>().velocity = transform.forward * shotSpeed * Time.deltaTime;
        //GetComponent<Rigidbody> ().AddForce (transform.forward * shotSpeed);
		//GetComponent<Rigidbody> ().AddForce((this.transform.localRotation * transform.forward) * shotSpeed);
		StartCoroutine (createMissile ());
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	IEnumerator createMissile(){		
		yield return new WaitForSeconds(3.0f);
		Destroy(gameObject);     
	}
}
