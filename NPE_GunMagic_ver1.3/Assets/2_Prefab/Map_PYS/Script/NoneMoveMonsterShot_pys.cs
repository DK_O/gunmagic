﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoneMoveMonsterShot_pys : MonoBehaviour {
    public GameObject Monster;
    public GameObject MonsterMissile;

    // Use this for initialization
    void Start () {
        InvokeRepeating("Shot", 1.0f, 2.0f);        
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void Shot()
    {
        Instantiate(MonsterMissile, transform.position, transform.rotation);
    }
}
