﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot_pys : MonoBehaviour {
	public GameObject Player;
	public GameObject PlayerMissile;
	public Transform ShootPos;
	public float shootDelay = 1.0f;
	float shootTimer = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Tab)) {
			PlayerShooting ();
		}
		
	}

    void PlayerShooting()
    {
        Instantiate(PlayerMissile, transform.position, transform.rotation);
    }

}
